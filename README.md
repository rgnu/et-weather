# ET-Weather

En una galaxia lejana, existen tres civilizaciones. Vulcanos, Ferengis y Betasoides.
Cada civilización vive en paz en su respectivo planeta.

Dominan la predicción del clima mediante un complejo sistema informático.

El sistema solar está conformado de la siguiente manera:
* El planeta Ferengi se desplaza con una velocidad angular de 1 grados/día en
sentido horario. Su distancia con respecto al sol es de 500Km.

* El planeta Betasoide se desplaza con una velocidad angular de 3 grados/día en
sentido horario. Su distancia con respecto al sol es de 2000Km.

* El planeta Vulcano se desplaza con una velocidad angular de 5 grados/día en
sentido anti­horario, su distancia con respecto al sol es de 1000Km.


## Premisas
* Todas las órbitas son circulares.

* Cuando los tres planetas están alineados entre sí y a su vez alineados con
respecto al sol, el sistema solar experimenta un período de sequía.

* Cuando los tres planetas no están alineados, forman entre sí un triángulo.
Es sabido que en el momento en el que el sol se encuentra dentro del triángulo,
el sistema solar experimenta un período de lluvia, teniendo éste, un pico de
intensidad cuando el perímetro del triángulo está en su máximo.

* Las condiciones óptimas de presión y temperatura se dan cuando los tres planetas
están alineados entre sí pero no están alineados con el sol.


Este servicio tiene como finalidad predecir los acontecimientos para los próximos 10 años:

1. ¿Cuántos períodos de sequía habrá?
2. ¿Cuántos períodos de lluvia habrá y qué día será el pico máximo de lluvia?
3. ¿Cuántos períodos de condiciones óptimas de presión y temperatura habrá?

Estas estadísticas podrán ser accedidas por medio de un API REST
Ej: GET → http://....../clima/stats → Respuesta: {"totalDroughtDays":40,"totalRainyDays":1208,"totalPTDays":20,"maxRainyDay":78}

Además todas las civilizaciones, podrán utilizar este servicio para conocer las
condiciones meteorológicas por medio de un API REST de consulta sobre las
condiciones de un día en particular.
Ej:   GET → http://....../clima?dia=566   → Respuesta: {"dia":566, "clima":"lluvia"}


## Tecnología utilizada
* java >= 7
* groovy 2.4 (Main application language)
* gradle 3.4 (Build Tool)
* springboot 1.5.8 (Web Framework)
* spock 1 (Testing and specification framework for Java and Groovy applications.)


### Comandos Utiles
- Build the application
```
$ ./gradlew assemble
```

- Run the Test Units
```
$ ./gradlew test
```

- Create documentation
```
$ ./gradlew groovydoc
```
