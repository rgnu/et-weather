package com.app.service

import spock.lang.*

import com.app.model.Point


class VulcanWeatherServiceSpec extends Specification {

  @Unroll
  void 'test getPrevision() args=#args expects=#expects'() {

    given: 'a VulcanWeatherService'
      def obj = new VulcanWeatherService()

    when:
      def result = obj.getPrevision(args.day)

    then: 'response should be instance of Map'
      result instanceof Map

    and:
      result == expects.result

    where:
      [args, expects] << [
        // Case 1: Ok
        [ [ day: 1], [ result: [day: 1, status:'normal', value:0] ] ],
        [ [ day: 90], [ result: [day: 90, status:'drought', value:1] ] ],
        [ [ day: 63], [ result: [day: 63, status:'optimumPT', value:1] ] ],
        [ [ day: 3488], [ result: [day:3488, status:'rainy', value:610400.0] ] ]
      ]
  }
}
