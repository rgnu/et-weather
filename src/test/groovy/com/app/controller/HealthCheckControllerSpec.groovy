package com.app.controller

import spock.lang.*

class HealthCheckControllerSpec extends Specification {

  HealthCheckController controller

  @Unroll
  void 'test handle() args=#args'() {

    given: 'A controller'
      controller = new HealthCheckController()

    when: 'call handle'
      Map response = controller.handle()

    then: 'response should be instance of Map'
      response instanceof Map

    and:
      response == expects.response

    where:
      [args, expects] << [
        // Case 1: Ok
        [
          [:],
          [response: [status: 'ok']]
        ]
      ]
  }
}
