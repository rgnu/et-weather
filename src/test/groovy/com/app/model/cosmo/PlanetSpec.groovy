package com.app.model.cosmo

import spock.lang.*

import com.app.model.Point


class PlanetSpec extends Specification {

  @Unroll
  void 'test getPosition() args=#args expects=#expects'() {

    given: 'a Planet'
      Planet obj    = new Planet(args)

    when: 'call getPosition'
      Point result = obj.getPosition()

    then: 'response should be instance of Point'
      result instanceof Point

    and:
      result == expects.result

    where:
      [args, expects] << [
        // Case 1: Ok
        [
          [ age: 180, speed: 1, distance: 500],
          [ result: new Point(x: -500, y: 0)]
        ],
        [
          [ age: 90, speed: 1, distance: 500],
          [ result: new Point(x: 0, y: 500)]
        ],
        // Case 2: Movimiento AntiHorario
        [
          [ age: 90, speed: -1, distance: 500],
          [ result: new Point(x: 0, y: -500)]
        ]

      ]
  }
}
