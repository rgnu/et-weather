package com.app.model

import spock.lang.*


class TriangleSpec extends Specification {

  @Unroll
  void 'test area() args=#args expects=#expects'() {

    given: 'a Triangle'
      Triangle t = new Triangle(args)

    when: 'call area()'
      Number result = t.area()

    then:
      result == expects.result

    where:
      [args, expects] << [
        // Case 1: Ok
        [
          [ p1: [x: 0, y: 0], p2: [x: 4, y: 0], p3: [x: 4, y: 4]],
          [ result: 8]
        ],
        [
          [ p1: [x: 0, y: 0], p2: [x: -4, y: 0], p3: [x: -4, y: 4]],
          [ result: 8]
        ],
        [
          [ p1: [x: 0, y: 7.483314], p2: [x: 0, y: 0], p3: [x: 6, y: 0]],
          [ result: 22.449942]
        ]
      ]
  }


  @Unroll
  void 'test contains() args=#args expects=#expects'() {

    given: 'a Triangle'
      Triangle t = new Triangle(args.triangle)

    and: 'a Point'
      Point p = new Point(args.point)

    when: 'call containt(p)'
      Boolean result = t.contains(p)

    then:
      result == expects.result

    where:
      [args, expects] << [
        // Case 1: Ok
        [
          [
            triangle: [p1: [x: 0, y: 0], p2: [x: 4, y: 0], p3: [x: 4, y: 4]],
            point: [x: 1.5, y: 0.5]
          ],
          [ result: true]
        ],
        // Case 2: false
        [
          [
            triangle: [p1: [x: 0, y: 0], p2: [x: 4, y: 0], p3: [x: 4, y: 4]],
            point: [x: -0.5, y: 0.5]
          ],
          [ result: false]
        ]
      ]
  }

}
