package com.app.model.weather

import spock.lang.*

import com.app.model.Point
import com.app.model.cosmo.System
import com.app.model.cosmo.Sun
import com.app.model.cosmo.Planet


class DroughtWeatherRuleSpec extends Specification {

  @Unroll
  void 'test getValue() args=#args expects=#expects'() {

    given: 'a System'
      System system = new System(
          time: args.time,
          bodies: [
            new Sun(),
            new Planet(speed: 1, distance: 500),
            new Planet(speed: 3, distance: 2000),
            new Planet(speed: 5, distance: 1000)
          ]
        )

    and: 'a DroughtWeatherRule'
      DroughtWeatherRule obj = new DroughtWeatherRule(system: system)

    when: 'call getValue'
      Number result = obj.getValue()

    then:
      result == expects.result

    where:
      [args, expects] << [
        // Case 1: OK
        [ [ time: 90], [ result: 1 ] ],
        // Case 2:
        [ [ time: 47], [ result: 0 ] ]
      ]
  }
}
