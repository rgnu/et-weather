package com.app.model.weather

import spock.lang.*

import com.app.model.Point
import com.app.model.cosmo.System
import com.app.model.cosmo.Sun
import com.app.model.cosmo.Planet


class OptimumPTWeatherRuleSpec extends Specification {

  @Unroll
  void 'test getValue() args=#args expects=#expects'() {

    given: 'a System'
      System system = new System(
          time: args.time,
          bodies: [
            new Sun(),
            new Planet(speed: 1, distance: 500),
            new Planet(speed: 3, distance: 2000),
            new Planet(speed: -5, distance: 1000)
          ]
        )

    and: 'a OptimumPTWeatherRule'
      OptimumPTWeatherRule obj    = new OptimumPTWeatherRule(system: system)

    when: 'call getValue'
      Number result = obj.getValue()

    then:
      result == expects.result

    where:
      [args, expects] << [
        // Case 1: Ok
        [ [ time: 1  ], [ result: 0 ] ],
        [ [ time: 90 ], [ result: 0 ] ],
        [ [ time: 63 ], [ result: 1 ] ]
      ]
  }
}
