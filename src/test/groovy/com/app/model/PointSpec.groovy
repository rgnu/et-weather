package com.app.model

import spock.lang.*


class PointSpec extends Specification {

  @Unroll
  void 'test minus() args=#args expects=#expects'() {

    given: 'Two point'
      Point p1 = new Point(args.p1)
      Point p2 = new Point(args.p2)

    when: 'do p1 - p2'
      Point result = p1 - p2

    then: 'response should be instance of Point'
      result instanceof Point

    and:
      result == expects.result

    where:
      [args, expects] << [
        // Case 1: Ok
        [
          [ p1: [x: 10, y: 10], p2: [x: 5, y: 15]],
          [ result: new Point(x: 5, y: -5)]
        ]
      ]
  }

  @Unroll
  void 'test plus() args=#args expects=#expects'() {

    given: 'Two point'
      Point p1 = new Point(args.p1)
      Point p2 = new Point(args.p2)

    when: 'do p1 + p2'
      Point result = p1 + p2

    then: 'response should be instance of Point'
      result instanceof Point

    and:
      result == expects.result

    where:
      [args, expects] << [
        // Case 1: Ok
        [
          [ p1: [x: 10, y: 10], p2: [x: 5, y: 15]],
          [ result: new Point(x: 15, y: 25)]
        ]
      ]
  }


  @Unroll
  void 'test collinear() args=#args expects=#expects'() {

    given: 'Three point'
      Point p1 = new Point(args.p1)
      Point p2 = new Point(args.p2)
      Point p3 = new Point(args.p3)

    when: 'call collinear()'
      Boolean result = p1.collinear(p2, p3)

    then: 'response should be instance of Bool'
      result instanceof Boolean

    and:
      result == expects.result

    where:
      [args, expects] << [
        // Case 1: Ok
        [
          [ p1: [x: 100, y: 0], p2: [x: 200, y: 0], p3: [x: 0, y: 0]],
          [ result: true]
        ],

        [
          [ p1: [x: 10, y: -15], p2: [x: 10, y: 0], p3: [x: 10, y: 15]],
          [ result: true]
        ],
        [
          [ p1: [x: -15, y: 10], p2: [x: 0, y: 10], p3: [x: 15, y: 10]],
          [ result: true]
        ],
        [
          [ p1: [x: -1, y: -1], p2: [x: 0, y: 0], p3: [x: 1, y: 1]],
          [ result: true]
        ],
        [
          [ p1: [x: 2, y: 4], p2: [x: 4, y: 6], p3: [x: 6, y: 8]],
          [ result: true]
        ],
        // Case 2: False
        [
          [ p1: [x: -1, y: -1], p2: [x: -10, y: 0], p3: [x: 1, y: 1]],
          [ result: false]
        ],
        [
          [ p1: [x: -1, y: -1], p2: [x: 0, y: 0], p3: [x: 0, y: 1]],
          [ result: false]
        ]
      ]
  }
}
