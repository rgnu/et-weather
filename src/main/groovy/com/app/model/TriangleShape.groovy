package com.app.model

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode
@ToString
class Triangle implements Shape {

  Point p1
  Point p2
  Point p3

  Number area() {
    Math.abs(( p1.x * (p2.y - p3.y) + p2.x * (p3.y - p1.y) + p3.x * (p1.y - p2.y))/2.0);
  }

  Boolean contains(Point p) {
    Triangle t1 = new Triangle(p1: p, p2: this.p2, p3: this.p3)
    Triangle t2 = new Triangle(p1: this.p1, p2: p, p3: this.p3)
    Triangle t3 = new Triangle(p1: this.p1, p2: this.p2, p3: p)

    t1.area() + t2.area() + t3.area() == this.area()
  }
}
