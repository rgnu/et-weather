package com.app.model

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode
@ToString
class Point {

  Number x
  Number y

  /**
   * Calculate the difference between two Points
   */
  Point minus(Point p) {
    new Point(x: this.x - p.x, y: this.y - p.y)
  }

  /**
   * Calculate the sum between two Points
   */
  Point plus(Point p) {
    new Point(x: this.x + p.x, y: this.y + p.y)
  }

  /**
   * Find if the points lie on a same straight line
   *
   * Slope formula method to find that points are collinear.
   */
  Boolean collinear(Point b, Point c) {

    Point ab = this - b
    Point bc = b - c

    Math.abs(ab.x * bc.y - bc.x * ab.y) == 0
  }
}
