package com.app.model

interface Shape {
  Number area()
}
