package com.app.model.weather

import com.app.model.cosmo.Body

abstract class DefaultWeatherRule implements WeatherRule {

  /**
   * Reference to the system we apply this rule
   */
  Body system

  /**
   * Get the calculated value to this rule, should be between 0 and 1
   */
  abstract Number getValue()
}
