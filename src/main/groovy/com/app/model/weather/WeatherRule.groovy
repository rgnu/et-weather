package com.app.model.weather

import com.app.model.cosmo.Body

interface WeatherRule {

  /**
   * Reference to the system we apply this rule
   */
  Body getSystem()

  /**
   * Get the calculated value to this rule
   */
  Number getValue()
}
