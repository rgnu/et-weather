package com.app.model.weather

import com.app.model.Point

class DroughtWeatherRule extends DefaultWeatherRule {

  /**
   * Get the calculated value to this rule,
   * it should be 1 when all Bodies are aligned and 0 otherwise
   */
  Number getValue() {

    if (system.bodies.size() < 3) return 0

    Point p1      = system.bodies.get(0).getPosition()
    Point p2      = system.bodies.get(1).getPosition()

    return system.bodies
    .takeWhile({ body ->
      body.getPosition().collinear(p1, p2)
    })
    .size() == system.bodies.size() ? 1 : 0
  }
}
