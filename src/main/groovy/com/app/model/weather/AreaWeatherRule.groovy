package com.app.model.weather

import com.app.model.Point
import com.app.model.Triangle
import com.app.model.cosmo.Planet

class AreaWeatherRule extends DefaultWeatherRule {

  /**
   * Get the calculated value to this rule
   */
  Number getValue() {

    Number result = 0
    Point p1,p2,p3

    system.getBodies()
    .grep { it instanceof Planet }
    .each {
      if (!p1) { p1 = it.getPosition(); return }
      if (!p2) { p2 = it.getPosition(); return }

      p3 = it.getPosition()

      result = new Triangle(p1: p1, p2: p2, p3: p3).area()
    }

    result
  }
}
