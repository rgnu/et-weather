package com.app.model.weather

import com.app.model.Point
import com.app.model.Triangle
import com.app.model.cosmo.Sun
import com.app.model.cosmo.Planet

class RainyWeatherRule extends DefaultWeatherRule {

  /**
   * Get the calculated value to this rule,
   * it should be > 0 when the Sun is inside Bodies and 0 otherwise
   */
  Number getValue() {

    Sun sun = system.bodies.grep({ it instanceof Sun }).getAt(0)

    if (!sun) return 0

    List<Planet>  planets = system.bodies.grep { it instanceof Planet }
    Point p1      = planets.get(0).getPosition()
    Point p2      = planets.get(1).getPosition()

    return planets
    .collect({ body ->
      Triangle t = new Triangle(p1: p1, p2: p2, p3: body.getPosition())

      if (!t.contains(sun.getPosition())) return 0

      t.area()
    })
    .inject { acc, val ->
      acc + val
    }
  }
}
