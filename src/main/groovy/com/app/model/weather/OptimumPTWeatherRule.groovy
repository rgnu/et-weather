package com.app.model.weather

import com.app.model.Triangle
import com.app.model.Point
import com.app.model.cosmo.Planet

class OptimumPTWeatherRule extends DroughtWeatherRule {

  /**
   * The minimum area to be consider
   */
  Number limit = 1000

  /**
   * Get the calculated value to this rule
   */
  Number getValue() {

    if (super.getValue()) return 0

    List<Planet>  planets = system.bodies.grep { it instanceof Planet }
    Number time           = system.time
    Number minArea        = getArea(planets)
    Number sign           = 1

    [0.5, 0.25, 0.125, 0.0625].each {
      system.time += (it * sign)
      Number area = getArea(planets)

      if (minArea < area ) {
        sign = -1
      } else {
        minArea = area
        sign = 1
      }
    }

    system.time = time

    minArea < limit ? 1 : 0
  }

  /**
   * Get the Triangle area formed by planets
   */
  Number getArea(planets) {
    Point p1      = planets.getAt(0)?.getPosition()
    Point p2      = planets.getAt(1)?.getPosition()

    return planets
    .collect({ body ->
      Triangle t = new Triangle(p1: p1, p2: p2, p3: body.getPosition())
      t.area()
    })
    .inject { acc, val ->
      acc + val
    }
  }
}
