package com.app.model.cosmo

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import com.app.model.Point

@EqualsAndHashCode
@ToString
abstract class DefaultBody implements Body {

  /**
   * Get the Name of this Celestial Body
   */
  String Name = 'undefined'

  /**
   * Initial Body age
   */
  Number age = 0

  /**
   * Get the System this Celestial Body below (can be null)
   */
  Body system = null

  /**
   * A list of Celestial Body (Planets or Moons) below to this (can be empty)
   */
  List<Body> bodies = []

  /**
   * Get the Current position of the Body
   */
  abstract Point getPosition()

  /**
   * Track the age of the system
   */
  Number getTime() {
    (system ? system.getTime() : 0) + age
  }

  /**
   * Set a list of Celestial Body (Planets or Moons) that below to this (can be empty)
   */
  void setBodies(List<Body> bodies) {
    bodies.each {
      it.system = this
    }
    this.bodies = bodies

  }
}
