package com.app.model.cosmo

import com.app.model.Point
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode
@ToString
class Planet extends DefaultBody {

  /**
   * Angular speed of planet in degrees by day
   */
  Number speed

  /**
   * Distance to the center orbit in kilometers
   */
  Number distance

  /**
   * Get the Current position of the Planet
   */
  @Override
  Point getPosition() {

    Number angle = (getTime() * speed)

    new Point(
      x: Math.cos(Math.toRadians(angle)).round(2) * distance,
      y: Math.sin(Math.toRadians(angle)).round(2) * distance
    )
  }
}
