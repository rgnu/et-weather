package com.app.model.cosmo


class VulcanSystem extends System {

  VulcanSystem() {
    super()

    bodies = [
      new Sun(),
      new Planet(name: 'Ferengi', speed: 1, distance: 500),
      new Planet(name: 'Betasoide', speed: 3, distance: 2000),
      new Planet(name: 'Vulcano', speed: -5, distance: 1000)
    ]
  }
}
