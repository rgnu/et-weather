package com.app.model.cosmo

import com.app.model.Point
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode
@ToString
class Sun extends DefaultBody {

  /**
   * Get the Current position of the Planet
   */
  @Override
  Point getPosition() {
    new Point(x: 0, y:0)
  }
}
