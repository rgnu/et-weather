package com.app.model.cosmo

import com.app.model.Point

interface Body {

  /**
   * Get the Name of this Celestial Body
   */
  String getName()

  /**
   * Get the System this Celestial Body below (can be null)
   */
  Body getSystem()

  /**
   * Get a list of Celestial Body (Planets or Moons) that below to this (can be empty)
   */
  List<Body> getBodies()

  /**
   * Get the Current position of the Body
   */
  Point getPosition()

  /**
   * Track the age of the system
   */
  Number getTime()
}
