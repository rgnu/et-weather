package com.app.model.cosmo

import com.app.model.Point
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode
@ToString
class System extends DefaultBody {

  /**
   * Track the age of the system
   */
  Number time

  /**
   * Get the current System age
   */
  @Override
  Number getTime() {
    time
  }

  /**
   * We assume default System position should be into center
   */
  @Override
  Point getPosition() {
    new Point(x: 0, y: 0)
  }
}
