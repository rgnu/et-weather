package com.app.controller

import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

import org.springframework.beans.factory.annotation.Autowired

import com.app.service.WeatherService

@RestController
class WeatherController {

  @Autowired
  WeatherService service

  Map weatherStatus = [
    normal:'normal',
    rainy: 'lluvia',
    drought: 'sequia',
    optimumPT: 'presion-temperatura-optima'
  ]

  @RequestMapping("/clima")
  Map prevision(@RequestParam(value = "dia", required = true) Number day) {
    [ dia: day, clima: weatherStatus[this.service.getPrevision(day)?.status]];
  }

  @RequestMapping("/clima/stats")
  Map stats() {
    [
      totalDroughtDays: this.service.getTotalDroughtDays(),
      totalRainyDays: this.service.getTotalRainyDays(),
      totalPTDays: this.service.getTotalPTDays(),
      maxRainyDay: this.service.getMaxRainyDay()
    ];
  }

}
