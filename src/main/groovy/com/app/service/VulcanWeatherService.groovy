package com.app.service

import com.app.model.weather.*
import com.app.model.cosmo.*

import org.springframework.stereotype.Service
import javax.annotation.PostConstruct

@Service
class VulcanWeatherService implements WeatherService {

  System system

  WeatherRule drought

  WeatherRule pt

  WeatherRule rainy

  Number totalDroughtDays    = 0
  Number totalRainyDays      = 0
  Number totalPTDays         = 0
  Number maxRainyDay         = null

  Number totalPrevisionDays  = 365 * 10

  VulcanWeatherService() {
    this.system    = new VulcanSystem()
    this.drought   = new DroughtWeatherRule(system: this.system)
    this.pt        = new OptimumPTWeatherRule(system: this.system)
    this.rainy     = new RainyWeatherRule(system: this.system)
  }

  /**
   * Calculate the previsions for the next N days
   */
  @PostConstruct
  void init() {

    Number maxRainy = 0
    Map    prevision

    (1..this.totalPrevisionDays).each { day ->
      prevision = getPrevision(day)

      if (prevision.status == 'drought') {
        totalDroughtDays++
      } else if (prevision.status == 'rainy') {
        totalRainyDays++
        if (prevision.value > maxRainy) {
          maxRainy    = prevision.value
          maxRainyDay = day
        }
      } else if (prevision.status ==  'optimumPT') {
        totalPTDays++
      }
    }
  }

  /**
   * Calculate the prevision
   */
  Map getPrevision(Number day) {

    Map prevision = [ day: day, status: 'normal' ]

    this.system.setTime(day)

    if ((prevision.value = rainy.getValue()) > 0) {
      prevision.status = 'rainy'
    } else if ((prevision.value = drought.getValue()) == 1) {
      prevision.status = 'drought'
    } else if ((prevision.value = pt.getValue()) == 1) {
      prevision.status = 'optimumPT'
    }

    prevision
  }

  /**
   * Return the total drought days
   */
  Map getPrevisionsStats() {
    [
      totalDroughtDays: this.totalDroughtDays,
      totalRainyDays: this.totalRainyDays,
      totalPTDays: this.totalPTDays,
      maxRainyDay: this.maxRainyDay
    ];
  }
}
