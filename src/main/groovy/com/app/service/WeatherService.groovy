package com.app.service

import com.app.model.weather.*
import com.app.model.cosmo.*

import org.springframework.stereotype.Service
import javax.annotation.PostConstruct

interface WeatherService {

  /**
   * Calculate the prevision
   */
  Map getPrevision(Number day)

  /**
   * Get the Calculated Previsions stats
   */
  Map getPrevisionsStats()

}
